provider "google" {
  credentials = file("/var/lib/jenkins/sc.json")
  project     = "netmagic-201406"
  region      = "us-central1"
}